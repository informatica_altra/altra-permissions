<?php 

namespace Altra\Permissions;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Altra\Permissions\Contracts\AltraPermissionInterface;
use Altra\Permissions\Traits\AltraPermissionTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class AltraPermission extends Model implements AltraPermissionInterface
{
    use AltraPermissionTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Creates a new instance of the model.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = Config::get('altra.permissions_table');
    }

}
