<?php

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

return [

  /*
  |--------------------------------------------------------------------------
  | Altra Role Model
  |--------------------------------------------------------------------------
  |
  | This is the Role model used by Altra to create correct relations.  Update
  | the role if it is in a different namespace.
  |
   */
  'role'                          => 'Altra\Permissions\AltraRole',

  /*
  |--------------------------------------------------------------------------
  | Altra Roles Table
  |--------------------------------------------------------------------------
  |
  | This is the roles table used by Altra to save roles to the database.
  |
   */
  'roles_table'                   => 'roles',

  /*
  |--------------------------------------------------------------------------
  | Altra role foreign key
  |--------------------------------------------------------------------------
  |
  | This is the role foreign key used by Altra to make a proper
  | relation between permissions and roles & roles and users
  |
   */
  'role_foreign_key'              => 'role_id',

  /*
  |--------------------------------------------------------------------------
  | Application User Model
  |--------------------------------------------------------------------------
  |
  | This is the User model used by Altra to create correct relations.
  | Update the User if it is in a different namespace.
  |
   */
  'user'                          => 'App\User',

  /*
  |--------------------------------------------------------------------------
  | Application Users Table
  |--------------------------------------------------------------------------
  |
  | This is the users table used by the application to save users to the
  | database.
  |
   */
  'users_table'                   => 'users',

  /*
  |--------------------------------------------------------------------------
  | Altra role_user Table
  |--------------------------------------------------------------------------
  |
  | This is the role_user table used by Altra to save assigned roles to the
  | database.
  |
   */
  'role_user_table'               => 'role_user',

  /*
  |--------------------------------------------------------------------------
  | Altra user foreign key
  |--------------------------------------------------------------------------
  |
  | This is the user foreign key used by Altra to make a proper
  | relation between roles and users
  |
   */
  'user_foreign_key'              => 'user_id',

  /*
  |--------------------------------------------------------------------------
  | Altra Permission Model
  |--------------------------------------------------------------------------
  |
  | This is the Permission model used by Altra to create correct relations.
  | Update the permission if it is in a different namespace.
  |
   */
  'permission'                    => 'Altra\Permissions\AltraPermission',

  /*
  |--------------------------------------------------------------------------
  | Altra Permissions Table
  |--------------------------------------------------------------------------
  |
  | This is the permissions table used by Altra to save permissions to the
  | database.
  |
   */
  'permissions_table'             => 'permissions',

  /*
  |--------------------------------------------------------------------------
  | Altra permission_role Table
  |--------------------------------------------------------------------------
  |
  | This is the permission_role table used by Altra to save relationship
  | between permissions and roles to the database.
  |
   */
  'permission_role_table'         => 'permission_role',

  /*
  |--------------------------------------------------------------------------
  | Altra permission foreign key
  |--------------------------------------------------------------------------
  |
  | This is the permission foreign key used by Altra to make a proper
  | relation between permissions and roles
  |
   */
  'permission_foreign_key'        => 'permission_id',

  /*
  |--------------------------------------------------------------------------
  | Altra Group Model
  |--------------------------------------------------------------------------
  |
  | This is the Group model used by Altra to create correct relations.
  | Update the group if it is in a different namespace.
  |
   */
  'group'                         => 'Altra\Permissions\AltraGroup',

  /*
  |--------------------------------------------------------------------------
  | Altra Groups Table
  |--------------------------------------------------------------------------
  |
  | This is the groups table used by Altra to save groups to the
  | database.
  |
   */
  'groups_table'                  => 'roles_groups',

  /*
  |--------------------------------------------------------------------------
  | Altra Group Translations Table
  |--------------------------------------------------------------------------
  |
  | This is the group translations table used by Altra to relate groups to group translations.
  |
   */
  'group_translations_table'      => 'roles_group_translations',

  /*
  |--------------------------------------------------------------------------
  | Altra group_role Table
  |--------------------------------------------------------------------------
  |
  | This is the group_role table used by Altra to save relationship
  | between roles and groups to the database.
  |
   */
  'group_role_table'              => 'role_roles_group',

  /*
  |--------------------------------------------------------------------------
  | Altra group foreign key
  |--------------------------------------------------------------------------
  |
  | This is the group foreign key used by Altra to make a proper
  | relation between groups and roles
  |
   */
  'group_foreign_key'             => 'roles_group_id',

  /*
  |--------------------------------------------------------------------------
  | Altra group translation foreign key
  |--------------------------------------------------------------------------
  |
  | This is the group translation foreign key used by Altra to make a proper
  | relation between groups and group_translations
  |
   */
  'group_translation_foreign_key' => 'roles_group_id',
];
