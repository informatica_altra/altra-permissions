<?php

namespace Altra\Permissions\CustomRelations;

use Altra\Permissions\AltraRole;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class UserPermissionRelation extends Relation
{

  public function __construct($parent)
  {
    parent::__construct(AltraRole::query(), $parent);
  }

  /**
   * Set the base constraints on the relation query.
   *
   * @return void
   */
  public function addConstraints()
  {
    /**
     * We make the query needed to bring all permissions from user
     */
    $this->query
      ->selectRaw('permissions.*, role_user.user_id, NULL roles_group_id')
      ->join('role_user', 'role_user.role_id', '=', 'roles.id')
      ->join('users', 'role_user.user_id', '=', 'users.id')
      ->join('permission_role', 'role_user.role_id', 'permission_role.role_id')
      ->join('permissions', 'permission_role.permission_id', 'permissions.id')
      ->distinct()
      ->union(
        DB::table('roles')
          ->selectRaw('permissions.*, NULL user_id, role_roles_group.roles_group_id AS roles_group_id')
          ->join('role_roles_group', 'roles.id', '=', 'role_roles_group.role_id')
          ->join('permission_role', 'roles.id', 'permission_role.role_id')
          ->join('permissions', 'permission_role.permission_id', 'permissions.id')
          ->distinct()
      )
    ;
  }

  /**
   * Set the constraints for an eager load of the relation.
   *
   * @param array $models
   *
   * @return void
   */
  public function addEagerConstraints(array $users)
  {
    /**
     * Eager load relation in roles
     */
    $this->query->whereIn(
      'user_id',
      collect($users)->pluck('id')
    );
  }

  /**
   * Initialize the relation on a set of models.
   *
   * @param array $models
   * @param string $relation
   *
   * @return array
   */
  public function initRelation(array $users, $relation)
  {
    /**
     * Create relation beetween user and roles
     */
    foreach ($users as $user) {
      $user->setRelation(
        $relation,
        $this->related->newCollection()
      );
    }

    return $users;
  }

  /**
   * Match the eagerly loaded results to their parents.
   *
   * @param array $models
   * @param \Illuminate\Database\Eloquent\Collection $users
   * @param string $relation
   *
   * @return array
   */
  function match ($users, Collection $permissions, $relation) {
    /**
     * We bring the permissions that belong to user
     */
    if ($permissions->isEmpty()) {
      return $users;
    }

    foreach ($users as $user) {
      $userGroupPermissions = $permissions->where('roles_group_id', $user->roles_group_id);
      $userRolesPermissions = $permissions->where('user_id', $user->id);
      $permissions          = $userGroupPermissions
        ->merge($userRolesPermissions)
        ->unique('id')
        ->sortBy('name')
        ->values();
      $user->setRelation($relation, $permissions);
    }
    return $users;
  }

  /**
   * Get the results of the relationship.
   *
   * @return mixed
   */
  public function getResults()
  {
    return $this->query->get();
  }
}
