<?php

namespace Altra\Permissions;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
class AltraGroupTranslation extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table;

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);
    $this->table = Config::get('altra.group_translations_table');

  }

}
