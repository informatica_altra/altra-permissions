<?php

namespace Altra\Permissions\Traits;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Illuminate\Cache\TaggableStore;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

trait AltraGroupTrait
{
  //Big block of caching functionality.
  public function cachedRoles()
  {
    $groupPrimaryKey = $this->primaryKey;
    $cacheKey        = 'altra_roles_for_group_' . $this->$groupPrimaryKey;
    if (Cache::getStore() instanceof TaggableStore) {
      return Cache::tags(Config::get('altra.role_group_table'))->remember($cacheKey, Config::get('cache.ttl', 60), function () {
        return $this->roles()->get();
      });
    } else {
      return $this->roles()->get();
    }

  }

  public function save(array $options = [])
  { //both inserts and updates
    if (!parent::save($options)) {
      return false;
    }
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_group_table'))->flush();
    }
    return true;
  }

  public function delete(array $options = [])
  { //soft or hard
    if (!parent::delete($options)) {
      return false;
    }
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_group_table'))->flush();
    }
    return true;
  }

  public function restore()
  { //soft delete undo's
    if (!parent::restore()) {
      return false;
    }
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_group_table'))->flush();
    }
    return true;
  }

  /**
   * One-to-Many relations with the user model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function users()
  {
    return $this->belongsToMany(Config::get('auth.providers.users.model'), Config::get('altra.group_user_table'), Config::get('altra.group_foreign_key'), Config::get('altra.user_foreign_key'));
  }

  /**
   * Has-Many-Through relation with the permission model.
   * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
   */
  public function perms()
  {
    return $this->hasManyThrough(Config::get('altra.permission'), Config::get('altra.permissions_table'), Config::get('altra.permission_role_table'));
  }

  /**
   * Many-to-Many relation with the role model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function roles()
  {
    return $this->belongsToMany(Config::get('altra.role'), Config::get('altra.group_role_table'), Config::get('altra.group_foreign_key'), Config::get('altra.role_foreign_key'));
  }

  /**
   * Boot the group model
   * Attach event listener to remove the many-to-many records when trying to delete
   * Will NOT delete any records if the role model uses soft deletes.
   *
   * @return void|bool
   */
  public static function boot()
  {
    parent::boot();

    static::deleting(function ($group) {
      if (!method_exists(Config::get('altra.group'), 'bootSoftDeletes')) {
        $group->users()->dissociate();
        $group->roles()->sync([]);
      }

      return true;
    });
  }

  /**
   * Checks if the group has a ROLE by its name.
   *
   * @param string|array $name rOLE name or array of role names.
   * @param bool $requireAll All roles in the array are required.
   *
   * @return bool
   */
  public function hasRole($name, $requireAll = false)
  {
    if (is_array($name)) {
      foreach ($name as $roleName) {
        $hasRole = $this->hasRole($roleName);

        if ($hasRole && !$requireAll) {
          return true;
        } elseif (!$hasRole && $requireAll) {
          return false;
        }
      }

      // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
      // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
      // Return the value of $requireAll;
      return $requireAll;
    } else {
      foreach ($this->cachedRoles() as $role) {
        if ($role->name == $name) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Save the inputted roles.
   *
   * @param mixed $inputRoles
   *
   * @return void
   */
  public function saveRoles($inputRoles)
  {
    if (!empty($inputRoles)) {
      $this->roles()->sync($inputRoles);
    } else {
      $this->roles()->detach();
    }

    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.group_role_table'))->flush();
    }
  }

  /**
   * Attach role to current group.
   *
   * @param object|array $role
   *
   * @return void
   */
  public function attachRole($role)
  {
    if (is_object($role)) {
      $role = $role->getKey();
    }

    if (is_array($role)) {
      return $this->attachRoles($role);
    }

    $this->roles()->attach($role);
  }

  /**
   * Detach role from current role.
   *
   * @param object|array $role
   *
   * @return void
   */
  public function detachRole($role)
  {
    if (is_object($role)) {
      $role = $role->getKey();
    }

    if (is_array($role)) {
      return $this->detachRoles($role);
    }

    $this->roles()->detach($role);
  }

  /**
   * Attach multiple roles to current group.
   *
   * @param mixed $roles
   *
   * @return void
   */
  public function attachRoles($roles)
  {
    foreach ($roles as $role) {
      $this->attachRole($role);
    }
  }

  /**
   * Detach multiple roles from current group
   *
   * @param mixed $roles
   *
   * @return void
   */
  public function detachRoles($roles = null)
  {
    if (!$roles) {
      $roles = $this->roles()->get();
    }

    foreach ($roles as $role) {
      $this->detachRole($role);
    }
  }
}
