<?php

namespace Altra\Permissions\Contracts;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

interface AltraGroupInterface
{
  /**
   * One-to-Many relations with the user model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function users();

  /**
   * Many-to-Many relations with the roles.
   *
   * @return \Illuminate\Database\Eloquent\Relations\hasMany
   */
  public function roles();

  /**
   * Many Through relation with the permission model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
   */
  public function perms();

  /**
   * Save the inputted roles.
   *
   * @param mixed $inputRoles
   *
   * @return void
   */
  public function saveRoles($inputRoles);

  /**
   * Attach role to current role.
   *
   * @param object|array $role
   *
   * @return void
   */
  public function attachRole($role);

  /**
   * Detach role form current group.
   *
   * @param object|array $role
   *
   * @return void
   */
  public function detachRole($role);

  /**
   * Attach multiple roles to current role.
   *
   * @param mixed $roles
   *
   * @return void
   */
  public function attachRoles($roles);

  /**
   * Detach multiple roles from current group
   *
   * @param mixed $roles
   *
   * @return void
   */
  public function detachRoles($roles);

}
