# Install instructions

Run composer require altra/permissions on terminal.

Publish the config file with

```php
php artisan vendor:publish
```


Publish migrations 

```php
php artisan altra:migration
```
