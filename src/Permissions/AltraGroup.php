<?php

namespace Altra\Permissions;

/**
 * This file is part of aLTRA,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Altra\Permissions\Contracts\AltraGroupInterface;
use Altra\Permissions\Traits\AltraGroupTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class AltraGroup extends Model implements AltraGroupInterface
{
  use AltraGroupTrait, Translatable;

  /**
   * Translation columns
   *
   * @var $translatedAttributes
   */
  public $translatedAttributes = [
    'name',
  ];

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table;

  /**
   * The translation foreign key used by the model.
   *
   * @var string
   */
  protected $translationForeignKey;

  /**
   * Creates a new instance of the model.
   *
   * @param array $attributes
   */
  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);
    $this->table                 = Config::get('altra.groups_table');
    $this->translationForeignKey = Config::get('altra.group_translation_foreign_key');
  }

}
