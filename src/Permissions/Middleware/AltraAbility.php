<?php 

namespace Altra\Permissions\Middleware;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AltraAbility
{
	const DELIMITER = '|';

	protected $auth;

	/**
	 * Creates a new instance of the middleware.
	 *
	 * @param Guard $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param Closure $next
	 * @param $roles
	 * @param $group
	 * @param $permissions
	 * @param bool $validateAll
	 * @return mixed
	 */
	public function handle($request, Closure $next, $roles, $permissions,$group, $validateAll = false)
	{
		if (!is_array($roles)) {
			$roles = explode(self::DELIMITER, $roles);
		}

		if (!is_array($group)) {
			$roles = explode(self::DELIMITER, $group);
		}

		if (!is_array($permissions)) {
			$permissions = explode(self::DELIMITER, $permissions);
		}

		if (!is_bool($validateAll)) {
			$validateAll = filter_var($validateAll, FILTER_VALIDATE_BOOLEAN);
		}

		if ($this->auth->guest() || !$request->user()->ability($roles, $permissions, $group, [ 'validate_all' => $validateAll ])) {
			abort(403);
		}

		return $next($request);
	}
}
