<?php 

namespace Altra\Permissions;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Illuminate\Support\ServiceProvider;

class AltraServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Publish config files
        $this->publishes([
            __DIR__.'/../config/config.php' => app()->basePath() . '/config/altra_permissions.php',
        ]);

        // Register commands
        $this->commands('command.altra.migration');

        // Register blade directives
        $this->bladeDirectives();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAltra();

        $this->registerCommands();

        $this->mergeConfig();
    }

    /**
     * Register the blade directives
     *
     * @return void
     */
    private function bladeDirectives()
    {
        if (!class_exists('\Blade')) return;

        // Call to Altra::hasRole
        \Blade::directive('role', function($expression) {
            return "<?php if (\\Altra::hasRole({$expression})) : ?>";
        });

        \Blade::directive('endrole', function($expression) {
            return "<?php endif; // Altra::hasRole ?>";
        });

        // Call to Altra::can
        \Blade::directive('permission', function($expression) {
            return "<?php if (\\Altra::can({$expression})) : ?>";
        });

        \Blade::directive('endpermission', function($expression) {
            return "<?php endif; // Altra::can ?>";
        });

        // Call to Altra::ability
        \Blade::directive('ability', function($expression) {
            return "<?php if (\\Altra::ability({$expression})) : ?>";
        });

        \Blade::directive('endability', function($expression) {
            return "<?php endif; // Altra::ability ?>";
        });
    }

    /**
     * Register the application bindings.
     *
     * @return void
     */
    private function registerAltra()
    {
        $this->app->bind('altra', function ($app) {
            return new Altra($app);
        });

        $this->app->alias('altra', 'AltraPermissions\Altra');
    }

    /**
     * Register the artisan commands.
     *
     * @return void
     */
    private function registerCommands()
    {
        $this->app->singleton('command.altra.migration', function ($app) {
            return new MigrationCommand();
        });
    }

    /**
     * Merges user's and altra's configs.
     *
     * @return void
     */
    private function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'altra'
        );
    }

    /**
     * Get the services provided.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'command.altra.migration'
        ];
    }
}
