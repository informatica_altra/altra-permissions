<?php

namespace Altra\Permissions\Traits;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Altra\Permissions\CustomRelations\RolesRelation;
use Altra\Permissions\CustomRelations\UserPermissionRelation;
use Illuminate\Cache\TaggableStore;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use InvalidArgumentException;
trait AltraUserTrait
{
  /**
   * Big block of caching functionality.
   *
   * @return mixed Roles
   */
  public function cachedRoles()
  {
    $userPrimaryKey = $this->primaryKey;
    $cacheKey       = 'altra_roles_for_user_' . $this->$userPrimaryKey;
    if (Cache::getStore() instanceof TaggableStore) {
      return Cache::tags(Config::get('altra.role_user_table'))->remember($cacheKey, Config::get('cache.ttl'), function () {
        return $this->roles;
      });
    } else {
      return $this->roles;
    }

  }

  /**
   * {@inheritDoc}
   */
  public function save(array $options = [])
  { //both inserts and updates
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_user_table'))->flush();
    }
    return parent::save($options);
  }

  /**
   * {@inheritDoc}
   */
  public function delete(array $options = [])
  { //soft or hard
    $result = parent::delete($options);
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_user_table'))->flush();
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function restore()
  { //soft delete undo's
    $result = parent::restore();
    if (Cache::getStore() instanceof TaggableStore) {
      Cache::tags(Config::get('altra.role_user_table'))->flush();
    }
    return $result;
  }

  /**
   * Many-to-Many relations with Role.
   *
   * @return Altra\Permissions\CustomRelations\RolesRelation
   */
  public function roles()
  {
    return new RolesRelation($this);
  }

  /**
   * One-to-Many relations with Permissions.
   *
   * @return Altra\Permissions\CustomRelations\RolesRelation
   */
  public function permissions()
  {
    return new UserPermissionRelation($this);
  }

  /**
   * One-to-Many relations with Group.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function group()
  {
    return $this->belongsTo(Config::get('altra.group'), Config::get('altra.group_foreign_key'));
  }

  /**
   * Boot the user model
   * Attach event listener to remove the many-to-many records when trying to delete
   * Will NOT delete any records if the user model uses soft deletes.
   *
   * @return void|bool
   */
  public static function boot()
  {
    parent::boot();

    static::deleting(function ($user) {
      if (!method_exists(Config::get('auth.providers.users.model'), 'bootSoftDeletes')) {
        $user->roles()->sync([]);
      }

      return true;
    });
  }

  /**
   * Checks if the user has a role by its name.
   *
   * @param string|array $name       Role name or array of role names.
   * @param bool         $requireAll All roles in the array are required.
   *
   * @return bool
   */
  public function hasRole($name, $requireAll = false)
  {
    if (is_array($name)) {
      foreach ($name as $roleName) {
        $hasRole = $this->hasRole($roleName);

        if ($hasRole && !$requireAll) {
          return true;
        } elseif (!$hasRole && $requireAll) {
          return false;
        }
      }

      // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
      // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
      // Return the value of $requireAll;
      return $requireAll;
    } else {
      foreach ($this->cachedRoles() as $role) {
        if ($role->name == $name) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Check if user has a permission by its name.
   *
   * @param string|array $permission Permission string or array of permissions.
   * @param bool         $requireAll All permissions in the array are required.
   *
   * @return bool
   */
  public function can($permission, $requireAll = false)
  {
    if (is_array($permission)) {
      foreach ($permission as $permName) {
        $hasPerm = $this->can($permName);

        if ($hasPerm && !$requireAll) {
          return true;
        } elseif (!$hasPerm && $requireAll) {
          return false;
        }
      }

      // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
      // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
      // Return the value of $requireAll;
      return $requireAll;
    } else {
      return (bool) ($this->permissions->where('name', $permission)->count());
    }

    return false;
  }

  /**
   * Checks role(s) and permission(s).
   *
   * @param string|array $roles       Array of roles or comma separated string
   * @param string|array $permissions Array of permissions or comma separated string.
   * @param array        $options     validate_all (true|false) or return_type (boolean|array|both)
   *
   * @throws \InvalidArgumentException
   *
   * @return array|bool
   */
  public function ability($roles, $permissions, $options = [])
  {
    // Convert string to array if that's what is passed in.
    if (!is_array($roles)) {
      $roles = explode(',', $roles);
    }
    if (!is_array($permissions)) {
      $permissions = explode(',', $permissions);
    }

    // Set up default values and validate options.
    if (!isset($options['validate_all'])) {
      $options['validate_all'] = false;
    } else {
      if ($options['validate_all'] !== true && $options['validate_all'] !== false) {
        throw new InvalidArgumentException();
      }
    }
    if (!isset($options['return_type'])) {
      $options['return_type'] = 'boolean';
    } else {
      if ($options['return_type'] != 'boolean' &&
        $options['return_type'] != 'array' &&
        $options['return_type'] != 'both') {
        throw new InvalidArgumentException();
      }
    }

    // Loop through roles and permissions and check each.
    $checkedRoles       = [];
    $checkedPermissions = [];
    foreach ($roles as $role) {
      $checkedRoles[$role] = $this->hasRole($role);
    }
    foreach ($permissions as $permission) {
      $checkedPermissions[$permission] = $this->can($permission);
    }

    // If validate all and there is a false in either
    // Check that if validate all, then there should not be any false.
    // Check that if not validate all, there must be at least one true.
    if (($options['validate_all'] && !(in_array(false, $checkedRoles) || in_array(false, $checkedPermissions))) ||
      (!$options['validate_all'] && (in_array(true, $checkedRoles) || in_array(true, $checkedPermissions)))) {
      $validateAll = true;
    } else {
      $validateAll = false;
    }

    // Return based on option
    if ($options['return_type'] == 'boolean') {
      return $validateAll;
    } elseif ($options['return_type'] == 'array') {
      return ['roles' => $checkedRoles, 'permissions' => $checkedPermissions];
    } else {
      return [$validateAll, ['roles' => $checkedRoles, 'permissions' => $checkedPermissions]];
    }

  }

  /**
   * Alias to eloquent many-to-many relation's attach() method.
   *
   * @param mixed $role
   */
  public function attachRole($role)
  {
    if (is_object($role)) {
      $role = $role->getKey();
    }

    if (is_array($role)) {
      $role = $role['id'];
    }

    $this->roles()->attach($role);
  }

  /**
   * Alias to eloquent many-to-many relation's detach() method.
   *
   * @param mixed $role
   */
  public function detachRole($role)
  {
    if (is_object($role)) {
      $role = $role->getKey();
    }

    if (is_array($role)) {
      $role = $role['id'];
    }

    $this->roles()->detach($role);
  }

  /**
   * Attach multiple roles to a user
   *
   * @param mixed $roles
   */
  public function attachRoles($roles)
  {
    foreach ($roles as $role) {
      $this->attachRole($role);
    }
  }

  /**
   * Detach multiple roles from a user
   *
   * @param mixed $roles
   */
  public function detachRoles($roles = null)
  {
    if (!$roles) {
      $roles = $this->roles()->get();
    }

    foreach ($roles as $role) {
      $this->detachRole($role);
    }
  }

  /**
   * Alias to eloquent one-to-many relation's associate() method.
   *
   * @param mixed $group
   */
  public function associateGroup($group)
  {
    if (is_object($group)) {
      $group = $group->getKey();
    }

    if (is_array($group)) {
      $group = $group['id'];
    }

    $this->group()->associate($group);
  }

  /**
   * Alias to eloquent one-to-many relation's dissociate() method.
   *
   * @param mixed $group
   */
  public function dissociateGroup($group)
  {
    if (is_object($group)) {
      $group = $group->getKey();
    }

    if (is_array($group)) {
      $group = $group['id'];
    }

    $this->group()->dissociate($group);
  }

  /**
   *Filtering users according to their role
   *
   *@param string $role
   *@return users collection
   */
  public function scopeWithRole($query, $role)
  {
    return $query->whereHas('roles', function ($query) use ($role) {
      $query->where('name', $role);
    });
  }

  /**
   *Filtering users according to their group
   *
   *@param string $group
   *@return users collection
   */
  public function scopeWithGroup($query, $group)
  {

    return $query->whereHas('group', function ($query) use ($group) {
      $query->whereTranslation('name', $group);
    });
  }

  /**
   *
   */
  public function getRolesAttribute()
  {
    if (isset($this->relations['roles'])) {
      return $this->relations['roles'];
    }

    return $this->load('roles')->relations['roles'];
  }

  /**
   * Get user permissions
   */
  public function getPermsAttribute()
  {
    return $this->roles->map(function ($role) {
      return $role->perms->makeHidden('pivot');
    })
      ->unique('id')
      ->sortBy('name')
      ->values();
  }

}
