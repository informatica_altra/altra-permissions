<?php

namespace Altra\Permissions\CustomRelations;

use Altra\Permissions\AltraRole;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class RolesRelation extends Relation
{

  public function __construct($parent)
  {
    parent::__construct(AltraRole::query(), $parent);
  }

  /**
   * Set the base constraints on the relation query.
   *
   * @return void
   */
  public function addConstraints()
  {
    /**
     * We make the query needed to bring all roles from user
     */
    $this->query
      ->selectRaw('roles.id, roles.name, roles.id AS role_id, role_user.user_id, NULL roles_group_id, roles.description AS description')
      ->join('role_user', 'role_user.role_id', '=', 'roles.id')
      ->join('users', 'role_user.user_id', '=', 'users.id')
      ->distinct()
      ->union(
        DB::table('roles')
          ->selectRaw('roles.id, roles.name, role_roles_group.role_id AS role_id, NULL user_id, role_roles_group.roles_group_id AS roles_group_id, roles.description AS description')
          ->join('role_roles_group', 'roles.id', '=', 'role_roles_group.role_id')
          ->distinct()
      );
  }

  /**
   * Set the constraints for an eager load of the relation.
   *
   * @param array $models
   *
   * @return void
   */
  public function addEagerConstraints(array $users)
  {
    /**
     * Eager load relation in roles
     */
    $this->query->whereIn(
      'user_id',
      collect($users)->pluck('id')
    );
  }

  /**
   * Initialize the relation on a set of models.
   *
   * @param array $models
   * @param string $relation
   *
   * @return array
   */
  public function initRelation(array $users, $relation)
  {
    /**
     * Create relation beetween user and roles
     */
    foreach ($users as $user) {
      $user->setRelation(
        $relation,
        $this->related->newCollection()
      );
    }

    return $users;
  }

  /**
   * Match the eagerly loaded results to their parents.
   *
   * @param array $models
   * @param \Illuminate\Database\Eloquent\Collection $users
   * @param string $relation
   *
   * @return array
   */
  function match($users, Collection $roles, $relation) {
    /**
     * We bring the roles that belong to user
     */
    if ($roles->isEmpty()) {
      return $users;
    }

    foreach ($users as $user) {
      $user->setRelation(
        $relation,
        $roles->filter(function ($role) use ($user) {
          return ($role->roles_group_id == $user->roles_group_id && is_null($role->user_id)) || ($role->user_id == $user->id);
        })
          ->unique('id')
          ->sortBy('name')
          ->values()
      );
    }

    return $users;
  }

  /**
   * Get the results of the relationship.
   *
   * @return mixed
   */
  public function getResults()
  {
    return $this->query->get();
  }

  /**
   * Attach a model to the parent.
   *
   * @param  mixed  $id
   * @return void
   */
  public function attach($id)
  {
    DB::table('role_user')->insertOrIgnore(['user_id' => $this->parent->id, 'role_id' => $id]);
  }
  /**
   * Detach a model to the parent.
   *
   * @param  mixed  $id
   * @return void
   */
  public function detach($id)
  {
    DB::table('role_user')->where('user_id', $this->parent->id)
      ->where('role_id', $id)
      ->delete();
  }

  /**
   * Sync a model to the parent.
   *
   * @param  array  $ids
   * @return void
   */
  public function sync(array $ids = [])
  {
    $ids = collect($ids);
    DB::table('role_user')->where('user_id', $this->parent->id)
      ->delete();
    if ($ids->isNotEmpty()) {
      $ids->transform(function ($id) {
        return ['user_id' => $this->parent->id, 'role_id' => $id];
      });

      DB::table('role_user')->where('user_id', $this->parent->id)
        ->insert($ids->toArray());

    }
  }
}
