<?php 

namespace Altra\Permissions;

/**
 * This file is part of aLTRA,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Altra\Permissions\Contracts\AltraRoleInterface;
use Altra\Permissions\Traits\AltraRoleTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class AltraRole extends Model implements AltraRoleInterface
{
    use AltraRoleTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Creates a new instance of the model.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = Config::get('altra.roles_table');
    }

}
