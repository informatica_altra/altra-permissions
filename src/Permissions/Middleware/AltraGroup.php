<?php 

namespace Altra\Permissions\Middleware;

/**
 * This file is part of Altra,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Altra\Permissions
 */

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AltraGroup
{
	const DELIMITER = '|';

	protected $auth;

	/**
	 * Creates a new instance of the middleware.
	 *
	 * @param Guard $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  Closure $next
	 * @param  $group
	 * @return mixed
	 */
	public function handle($request, Closure $next, $group)
	{
		if (!is_array($group)) {
			$roles = explode(self::DELIMITER, $group);
		}

		if ($this->auth->guest() || !$request->user()->hasGroup($group)) {
			abort(403);
		}

		return $next($request);
	}
}
