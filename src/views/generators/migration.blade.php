<?php echo '<?php' ?>

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AltraPermissionsSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        // Create table for storing roles
        Schema::create('{{ $rolesTable }}', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('{{ $roleUserTable }}', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('{{ $userKeyName }}')->on('{{ $usersTable }}')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('{{ $rolesTable }}')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('{{ $permissionsTable }}', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('{{ $permissionRoleTable }}', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('{{ $permissionsTable }}')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('{{ $rolesTable }}')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        // Create table for storing groups
        Schema::create('{{ $groupsTable }}', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_public');
            $table->timestamps();
        });

        // Create table for storing groups translations
        Schema::create('{{ $groupTranslationsTable }}', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('roles_group_id')->unsigned();
            $table->foreign('roles_group_id')->references('id')->on('{{ $groupsTable }}')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('locale');
            $table->string('name')->unique();
            $table->longText('description');
            $table->timestamps();
        });

        // Create table for associating groups to roles (Many-to-Many)
        Schema::create('{{ $groupRoleTable }}', function (Blueprint $table) {
            $table->integer('roles_group_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('roles_group_id')->references('id')->on('{{ $groupsTable }}')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('{{ $rolesTable }}')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['roles_group_id', 'role_id']);
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('{{ $permissionRoleTable }}');
        Schema::drop('{{ $permissionsTable }}');
        Schema::drop('{{ $roleUserTable }}');
        Schema::drop('{{ $rolesTable }}');
        Schema::drop('{{ $groupsTable }}');
        Schema::drop('{{ $groupTranslationsTable }}');
        Schema::drop('{{ $groupRoleTable }}');

    }
}
